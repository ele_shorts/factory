package main

import (
	"errors"
	"log"
	"time"
)

type (
	factory interface {
		say()
	}
	foo struct{}
	bar struct{}
)

func (f *foo) say() {
	log.Println("FOO")
}

func (b *bar) say() {
	log.Println("BAR")
}

func new(i int) (factory, error) {
	switch i % 3 {
	case 0:
		return &foo{}, nil
	case 1:
		return &bar{}, nil
	default:
		return nil, errors.New("provider not found")
	}
}

func main() {
	for i := range "foo&bar" {
		func() {
			defer time.Sleep(time.Millisecond * 500)
			f, err := new(i)
			if err != nil {
				log.Println("error: ", err)
				return
			}
			f.say()
		}()
	}
}
